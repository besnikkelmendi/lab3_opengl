#include "renderer.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include <GLFW/glfw3.h>

int main(void)
{
    //auto t_start = std::chrono::high_resolution_clock::now();
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    // Setting window hints
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    /* Create a windowed mode window and its OpenGL context */
    GLuint width = 1080;
    GLuint height = 1080;
    window = glfwCreateWindow(width, height, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;

    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (glewInit() != GLEW_OK)
        std::cout << "Error!" << std::endl;
    std::cout << glGetString(GL_VERSION) << std::endl;

    // Eanable capture of debug output.
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(MessageCallback, 0);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    GLfloat rect[20] =
    {
    -0.25f, 0.25f, 1.0f, 0.0f, 0.0f,
    -0.25f, -0.25f, 0.0f,1.0f, 0.0f,
    0.25f, -0.25f, 0.0f,0.0f, 1.0f,
    0.25f, 0.25f, 1.0f,1.0f, 0.0f,
    };
        
    GLuint rect_indices[6] = {0,1,2,0,2,3};
        
    GLuint vao;
    glCreateVertexArrays(1, &vao);
    glBindVertexArray(vao);
        
    VertexBuffer vb(rect, sizeof(rect));
    IndexBuffer ib(rect_indices,6);
    vb.Attach();
    ib.Attach();
    
    //auto RVAO = DrawRectangle();
    std::string spath = "res/shaders/rectangle.shader";
    Shader shader(spath);
    
    // Specify the layout of the vertex data
    GLint posAttrib = glGetAttribLocation(shader.RendererID, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

    GLint colAttrib = glGetAttribLocation(shader.RendererID, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));
    
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    double currentTime = 0.0;
    glfwSetTime(0.0);
    glm::mat4 trans = glm::mat4(1.0f);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Poll for and process events */
        glfwPollEvents();

        currentTime = glfwGetTime();
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT);
        shader.Attach();
        glBindVertexArray(vao);
        shader.SetUniform4f("scaleColor", 1.0f);
        // Calculate transformation
       
        
        trans = glm::scale(
            trans,
            glm::vec3(1.00025f, 1.0f, 0.0f)
        );
        trans = glm::translate(
            trans, 
            glm::vec3(-0.01f, 0.0f, 0.0f)
        );
        
        trans = glm::rotate(
            trans,
            glm::radians(float(currentTime)) * 10,
            glm::vec3(0.0f, 0.0f, 1.0f)
        );
        shader.SetUniformMatrix4fv("trans", 1, GL_FALSE, trans);
        
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT,0);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {
            break;
        }


    }

    glfwTerminate();
    return 0;
}